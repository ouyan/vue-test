import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Name from '@/components/name'
import Mess from '@/components/children/mess'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/name',
      name: 'Ouyang',
      component: Name,
      children: [
        {
          path: 'profile',
          name: 'mess',
          component: Mess,
        }
      ]
    }
  ]
})
